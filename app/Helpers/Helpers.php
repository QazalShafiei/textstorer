<?php
namespace App\Helpers;

class Helpers
{
    public static function saveUserLogs($userData) : void
    {
        $file = fopen("UserLogs.txt", 'ab') or die("Unable to open UserLogs file!");
        fwrite($file, (collect($userData))->implode(",") . PHP_EOL);
        fclose($file);
    }

    public static function userDataValidation($userData) : bool
    {
        if (empty($userData) || !isset($userData[0]) || !isset($userData[1])
            || !is_numeric($userData[0]) || !is_numeric($userData[1])) {
                self::saveUserLogs($userData);
                return true;
            }
            return false;
    }
}
?>
