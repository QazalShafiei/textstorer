<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helpers;
use Exception;

class StoreUsersInformationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:store-users-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store users data from specific .txt file in users table';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Add users.txt file to /storage/app! example file is exists in repo.
            $usersData = explode("\n", Storage::get('users.txt'));
            $bar = $this->output->createProgressBar(count($usersData));
            $data = array();

            $bar->start();
            foreach ($usersData as $userData) {

                if (!preg_match("/\t/", $userData)) {
                    Helpers::saveUserLogs($userData);
                    continue;
                }

                $userData = explode("\t", $userData);

                if (Helpers::userDataValidation($userData)) {
                    next($userData);
                    continue;
                }

                $data[] = [
                    "national_code" => $userData[1],
                    "phone" => $userData[0],
                ];

                $bar->advance();
            }

            DB::table('users')->insert($data);
            $bar->finish();
            $this->info("\n all users data imported successfully!");
    }
}
